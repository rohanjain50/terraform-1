module "gke-cluster" {
  source = "google-terraform-modules/kubernetes-engine/google"
  version = "1.19.1"

  general = {
    name = "mycluster"
    env  = "prod"
    zone = "europe-west1-b"
  }

  master = {}
}
