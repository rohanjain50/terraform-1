variable "project_id" {
  description = "The unique id for the project"
  default ="tonal-plateau-212205"
}
variable "cluster_name" {
  description = "The human readable name for the project"
  default = "kube"
}

variable "terraform_admin_key" {
  description = "The SA key for the Terraform Admin account for the target project"
}
variable "gke-service-account" {
  description = "The SA key for the Terraform Admin account for the target project"
  default = "gke-service-account"
}

variable "region" {
  description = "The default region for regional project resources"
  default     = "europe-west2"
}
variable "zone" {
  description = "The default zone for zonal project resources"
  default     = ["europe-west2-b", "europe-west2-a" ]
}
variable "project_zone" {
  description = "The default zone for zonal project resources"
  default     = "europe-west2-b"
}

variable "gke_service_account_role" {
  description = "The default zone for zonal project resources"
  default     = "roles/container.developer"
}


