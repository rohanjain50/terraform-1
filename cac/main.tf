# terraform main script
terraform {
  backend "gcs" {
     bucket = "tf-state-storage1"
     prefix = "awsome/cac"
     credentials = "tf-state-key.json"
     project = "tonal-plateau-212205"
     region =  "europe_west2"
    }
}
# configure the Google Cloud provider
 provider "google" {
   version =  "1.18.0"
   project =  "${var.project_id}"
   region =  "${var.region}"
   zone = "${var.project_zone}"
   credentials =  "${var.terraform_admin_key}"
}


resource "google_service_account" "gke_sa" {
  account_id   = "${var.gke-service-account}"
  display_name = "Terraform managed"
}
/*
data "google_iam_policy" "sa_user" {
  binding {
    role = "${var.gke_service_account_role}"
    members = [
      "serviceAccount:${google_service_account.gke_sa.email}",
    ]
  }
}
resource "google_service_account_iam_policy" "gke_account_iam" {
  service_account_id = "${google_service_account.gke_sa.id}"
  policy_data        = "${data.google_iam_policy.sa_user.policy_data}"
}
*/


resource "kubernetes_deployment" "null" {
  metadata {
    name = "nginx-1"
    labels {
      app = "nginx-1"
    }
  }
  spec {
    replicas = 2
    min_ready_seconds = 40
    selector {
      match_labels {
        app = "nginx-1"
      }
    }
    template {
      metadata {
        labels {
          app = "nginx-1"
        }
      }
      spec {
	termination_grace_period_seconds = 120
        container {
          image = "nginx:latest"
          name  = "nginx-1"
        }
      }
    }
  }
depends_on = ["null_resource.delay"]
}


resource "kubernetes_service" "kubernetes_deployment" {
  metadata {
    name = "nginx-1-service"
    namespace = "default"
    labels {
          app = "nginx-1"
        }
  }
  spec {
    selector {
      app = "nginx-1"
    }
    session_affinity = "ClientIP"
    port {
      port = 8080
      target_port = 80
      protocol = "TCP"
    }
    type = "LoadBalancer"
  }
depends_on = ["kubernetes_deployment.null"]
}

resource "null_resource" "kubeconfig" {
  provisioner "remote-exec" {
    command = <<LOCAL_EXEC
gcloud container clusters get-credentials mycluster-prod-europe-west1-b-master --zone europe-west1-b --project tonal-plateau-212205
LOCAL_EXEC
  }
depends_on = ["module.gke-cluster"]
}

resource "null_resource" "delay" {
  provisioner "local-exec" {
    command = "sleep 10"
  }
  depends_on = ["null_resource.kubeconfig"]
  }
